/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include "sphvr-video.h"
#include "sphvr-pipeline.h"

#include <stdbool.h>
#include <stdio.h>

#include <gst/gst.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <gst/vulkan/gstvkimagememory.h>
#pragma GCC diagnostic pop

struct _SphvrVideo
{
  GObject parent;

  GstElement *playbin;

  gboolean playing;
  gboolean seek_enabled;

  gint64 duration;

  guint sourceid;

  VkSampler sampler;

  SphvrRenderer *renderer;
};

G_DEFINE_TYPE (SphvrVideo, sphvr_video, G_TYPE_OBJECT)

static void
sphvr_video_init (SphvrVideo *self)
{
  (void) self;
}

SphvrVideo *
sphvr_video_new (void)
{
  return (SphvrVideo *) g_object_new (SPHVR_TYPE_VIDEO, 0);
}

static void
_finalize (GObject *gobject)
{
  SphvrVideo *self = SPHVR_VIDEO (gobject);
  gst_element_set_state (self->playbin, GST_STATE_NULL);
  gst_object_unref (self->playbin);

  GulkanContext *gulkan = gulkan_renderer_get_context (
    GULKAN_RENDERER (self->renderer));
  VkDevice device = gulkan_context_get_device_handle (gulkan);

  vkDestroySampler (device, self->sampler, NULL);
  g_object_unref (self->renderer);
  G_OBJECT_CLASS (sphvr_video_parent_class)->finalize (gobject);
}

static void
sphvr_video_class_init (SphvrVideoClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;
}

static VkImageView last_image_view = VK_NULL_HANDLE;

static gboolean
_init_sampler (VkDevice device, VkSampler *sampler)
{
  VkSamplerCreateInfo info = {
    .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
    .magFilter = VK_FILTER_LINEAR,
    .minFilter = VK_FILTER_LINEAR,
    .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
    .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
    .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
    .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
    .anisotropyEnable = VK_FALSE,
    .maxAnisotropy = 0,
    .compareEnable = VK_FALSE,
    .compareOp = VK_COMPARE_OP_ALWAYS,
    .borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK,
    .unnormalizedCoordinates = VK_FALSE,
  };

  VkResult res = vkCreateSampler (device, &info, NULL, sampler);
  vk_check_error ("vkCreateSampler", res, FALSE);

  return TRUE;
}

/* The appsink has received a buffer */
static GstFlowReturn
_sample_cb (GstElement *sink, SphvrVideo *self)
{
  GstSample *sample;

  /* Retrieve the buffer */
  g_signal_emit_by_name (sink, "pull-sample", &sample);
  if (sample) {
    GstBuffer *buffer = gst_sample_get_buffer (sample);
    // guint memcount = gst_buffer_n_memory (buffer);
    guint i = 0;
    // for (guint i = 0; i < memcount; i++) {
    GstMemory *mem = gst_buffer_peek_memory (buffer, i);
    // printf("memory #%d ", i);

    if (gst_is_vulkan_image_memory (mem)) {
      GstVulkanImageMemory *im = (GstVulkanImageMemory *) mem;
      /*
      VkImage image = im->image;
      guint32 w = gst_vulkan_image_memory_get_width (im);
      guint32 h = gst_vulkan_image_memory_get_height (im);
      */
      // printf("%p %dx%d\n", (void*) &image,w , h);
      //
      // printf ("gst device handle: %p\n", (void*) im->device->device);

      // GstVulkanImageView *view = gst_vulkan_get_or_create_image_view (im);
      // self->update_image_view_cb (self->renderer, view->view);
      //
      //
      VkImageViewCreateInfo image_view_info
        = {.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
           .image = im->image,
           .viewType = VK_IMAGE_VIEW_TYPE_2D,
           .format = VK_FORMAT_R8G8B8A8_SRGB,
           .subresourceRange = {
             .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
             .baseMipLevel = 0,
             .levelCount = 1,
             .baseArrayLayer = 0,
             .layerCount = 1,
           }};

      GulkanContext *gulkan = gulkan_renderer_get_context (
        GULKAN_RENDERER (self->renderer));
      VkDevice device = gulkan_context_get_device_handle (gulkan);

      bool first_run = last_image_view == VK_NULL_HANDLE;

      if (!first_run) {
        vkDestroyImageView (device, last_image_view, NULL);
      }

      VkResult res = vkCreateImageView (device, &image_view_info, NULL,
                                        &last_image_view);
      vk_check_error ("vkCreateImageView", res, GST_FLOW_ERROR);

      SphvrPipeline *pipeline = sphvr_renderer_get_pipeline (self->renderer);

      sphvr_pipeline_set_image_view (pipeline, last_image_view, self->sampler);

      if (first_run)
        if (!sphvr_renderer_cache_draw_commands (self->renderer))
          g_warning ("Could not recreate command buffers.");
    }

    gst_sample_unref (sample);
    return GST_FLOW_OK;
  }

  return GST_FLOW_ERROR;
}

static void
_error_cb (GstBus *bus, GstMessage *msg, SphvrVideo *data)
{
  (void) bus;
  (void) data;

  GError *err;
  gchar *debug_info;

  /* Print error details on the screen */
  gst_message_parse_error (msg, &err, &debug_info);
  g_printerr ("Error received from element %s: %s\n",
              GST_OBJECT_NAME (msg->src), err->message);
  g_printerr ("Debugging information: %s\n", debug_info ? debug_info : "none");
  g_clear_error (&err);
  g_free (debug_info);
}

void
sphvr_video_set_renderer (SphvrVideo *self, SphvrRenderer *renderer)
{
  self->renderer = g_object_ref (renderer);
}

static void
_state_changed_cb (GstBus *bus, GstMessage *msg, SphvrVideo *self)
{
  (void) bus;
  GstState old_state, new_state, pending_state;
  gst_message_parse_state_changed (msg, &old_state, &new_state,
                                   &pending_state);

  if (GST_MESSAGE_SRC (msg) == GST_OBJECT (self->playbin)) {
    g_print ("Pipeline state changed from %s to %s:\n",
             gst_element_state_get_name (old_state),
             gst_element_state_get_name (new_state));

    /* Remember whether we are in the PLAYING state or not */
    self->playing = (new_state == GST_STATE_PLAYING);

    if (new_state == GST_STATE_PAUSED) {
      GST_DEBUG_BIN_TO_DOT_FILE (GST_BIN (self->playbin),
                                 GST_DEBUG_GRAPH_SHOW_ALL, "paused");
    } else if (new_state == GST_STATE_READY) {
      GST_DEBUG_BIN_TO_DOT_FILE (GST_BIN (self->playbin),
                                 GST_DEBUG_GRAPH_SHOW_ALL, "ready");
    }

    if (self->playing) {
      GST_DEBUG_BIN_TO_DOT_FILE (GST_BIN (self->playbin),
                                 GST_DEBUG_GRAPH_SHOW_ALL, "playing");

      /* We just moved to PLAYING. Check if seeking is possible */
      GstQuery *query;
      gint64 start, end;
      query = gst_query_new_seeking (GST_FORMAT_TIME);
      if (gst_element_query (self->playbin, query)) {
        gst_query_parse_seeking (query, NULL, &self->seek_enabled, &start,
                                 &end);
        if (self->seek_enabled) {
          g_print ("Seeking is ENABLED from %" GST_TIME_FORMAT
                   " to %" GST_TIME_FORMAT "\n",
                   GST_TIME_ARGS (start), GST_TIME_ARGS (end));
        } else {
          g_print ("Seeking is DISABLED for this stream.\n");
        }
      } else {
        g_printerr ("Seeking query failed.");
      }
      gst_query_unref (query);
    }
  }
}

bool
sphvr_video_initialize (SphvrVideo *self, const char *uri)
{
  GstBus *bus;

  GstElement *appsink, *vulkanupload, *capsfilter, *vulkan_app_sink;

  /* Create the elements */
  self->playbin = gst_element_factory_make ("playbin", "playbin");
  vulkanupload = gst_element_factory_make ("vulkanupload", "vulkanupload");
  appsink = gst_element_factory_make ("appsink", "appsink");
  capsfilter = gst_element_factory_make ("capsfilter", "capsfilter");

  GstElement *videoconvert = gst_element_factory_make ("videoconvert",
                                                       "videoconvert");

  /* Create the empty pipeline */
  if (!appsink) {
    g_printerr ("Not all elements could be created.\n");
    return false;
  }

  g_object_set (self->playbin, "uri", uri, NULL);

  GstCaps *vkimagecaps = gst_caps_new_simple ("video/x-raw", "format",
                                              G_TYPE_STRING, "RGBA", NULL);

  GstCapsFeatures *vkimagefeatures = gst_caps_features_new ("memory:"
                                                            "VulkanImage",
                                                            NULL);

  gst_caps_set_features (vkimagecaps, 0, vkimagefeatures);
  g_object_set (capsfilter, "caps", vkimagecaps, NULL);

  vulkan_app_sink = gst_bin_new ("vulkanappsink");

  gst_bin_add_many (GST_BIN (vulkan_app_sink), videoconvert, vulkanupload,
                    capsfilter, appsink, NULL);
  gst_element_link_many (videoconvert, vulkanupload, capsfilter, appsink,
                         NULL);

  /* Configure appsink */
  g_object_set (appsink, "emit-signals", TRUE, NULL);
  g_signal_connect (appsink, "new-sample", G_CALLBACK (_sample_cb), self);

  GstPad *pad = gst_element_get_static_pad (videoconvert, "sink");
  GstPad *ghost_pad = gst_ghost_pad_new ("sink", pad);
  gst_pad_set_active (ghost_pad, TRUE);
  gst_element_add_pad (vulkan_app_sink, ghost_pad);
  gst_object_unref (pad);
  g_object_set (self->playbin, "video-sink", vulkan_app_sink, NULL);

  /* Instruct the bus to emit signals for each received message, and connect to
   * the interesting signals */
  bus = gst_element_get_bus (self->playbin);
  gst_bus_add_signal_watch (bus);
  g_signal_connect (G_OBJECT (bus), "message::error", G_CALLBACK (_error_cb),
                    self);
  g_signal_connect (G_OBJECT (bus), "message::state-changed",
                    G_CALLBACK (_state_changed_cb), self);
  gst_object_unref (bus);

  /* Start playing the pipeline */
  gst_element_set_state (self->playbin, GST_STATE_PLAYING);

  GulkanContext *gulkan = gulkan_renderer_get_context (
    GULKAN_RENDERER (self->renderer));
  VkDevice device = gulkan_context_get_device_handle (gulkan);

  _init_sampler (device, &self->sampler);

  return true;
}

void
sphvr_video_toggle_pause (SphvrVideo *self)
{
  if (self->playing)
    gst_element_set_state (self->playbin, GST_STATE_PAUSED);
  else
    gst_element_set_state (self->playbin, GST_STATE_PLAYING);
}

void
sphvr_video_seek (SphvrVideo *self, int seconds)
{
  gint64 current = -1;
  /* Query the current position of the stream */
  if (!gst_element_query_position (self->playbin, GST_FORMAT_TIME, &current)) {
    g_printerr ("Could not query current position.\n");
  }

  gint64 seek_pos = MAX (0, current + seconds * GST_SECOND);

  printf ("Seeking to %ld\n", seek_pos);

  gst_element_seek_simple (self->playbin, GST_FORMAT_TIME,
                           GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_KEY_UNIT,
                           seek_pos);
}
