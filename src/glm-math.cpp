#include "glm-math.h"

#define GLM_FORCE_CTOR_INIT

#include <glm/glm.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <math.h>

/*
static glm::mat4
graphene_to_glm_matrix (const graphene_matrix_t *in)
{
  float matrix[16];
  graphene_matrix_to_float (in, matrix);
  return glm::make_mat4 (matrix);
}
*/

static void
glm_to_graphene_matrix (const glm::mat4 &mat, graphene_matrix_t *out)
{
  graphene_matrix_init_from_float (out, glm::value_ptr (mat));
}

void
glm_sphere_view (float theta, float phi, graphene_matrix_t *out)
{
  glm::vec3 center = glm::vec3 (sin (theta) * cos (phi), cos (theta),
                                sin (theta) * sin (phi));

  glm::vec3 eye = glm::vec3 (0, 0, 0);
  glm::vec3 up = glm::vec3 (0, 1, 0);

  glm::mat4 view = glm::lookAt (eye, center, up);

  glm_to_graphene_matrix (view, out);
}
