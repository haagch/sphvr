/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <glib-object.h>
#include <gulkan.h>

#include "sphvr-enums.h"

#define SPHVR_TYPE_RENDERER sphvr_renderer_get_type ()
G_DECLARE_INTERFACE (SphvrRenderer, sphvr_renderer, SPHVR, RENDERER, GObject)

#define SPHVR_TYPE_RENDERER_DESKTOP sphvr_renderer_desktop_get_type ()
G_DECLARE_FINAL_TYPE (SphvrRendererDesktop, sphvr_renderer_desktop, SPHVR,
                      RENDERER_DESKTOP, GulkanSwapchainRenderer)

#define SPHVR_TYPE_RENDERER_XR sphvr_renderer_xr_get_type ()
G_DECLARE_FINAL_TYPE (SphvrRendererXr, sphvr_renderer_xr, SPHVR, RENDERER_XR,
                      GulkanRenderer)

typedef struct _SphvrPipeline SphvrPipeline;

struct _SphvrRendererInterface
{
  GTypeInterface parent_iface;

  GulkanRenderer *(*get_renderer) (SphvrRenderer *self);

  SphvrPipeline *(*get_pipeline) (SphvrRenderer *self);

  void (*set_pipeline) (SphvrRenderer *self, SphvrPipeline *pipeline);

  gboolean (*cache_draw_commands) (SphvrRenderer *self);

  void (*draw) (SphvrRenderer *self);

  gboolean (*initialize) (SphvrRenderer *self);
};

gboolean
sphvr_renderer_initialize (SphvrRenderer *self);

void
sphvr_renderer_draw (SphvrRenderer *self);

SphvrPipeline *
sphvr_renderer_get_pipeline (SphvrRenderer *self);

void
sphvr_renderer_set_pipeline (SphvrRenderer *self, SphvrPipeline *pipeline);

GulkanRenderer *
sphvr_renderer_get_renderer (SphvrRenderer *self);

gboolean
sphvr_renderer_cache_draw_commands (SphvrRenderer *self);
