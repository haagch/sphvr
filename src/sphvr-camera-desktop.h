/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef SPHVR_CAMERA_DESKTOP_H_
#define SPHVR_CAMERA_DESKTOP_H_

#include <glib-object.h>
#include <graphene.h>

#define SPHVR_TYPE_CAMERA_DESKTOP sphvr_camera_desktop_get_type ()
G_DECLARE_FINAL_TYPE (SphvrCameraDesktop, sphvr_camera_desktop, SPHVR,
                      CAMERA_DESKTOP, GObject)

struct _SphvrCameraDesktop
{
  GObject parent;

  graphene_matrix_t mvp;

  /* position */
  graphene_vec3_t eye;
  graphene_vec3_t center;
  graphene_vec3_t up;

  GList *pushed_buttons;

  /* perspective */
  gfloat fov;
  gfloat aspect;
  gfloat znear;
  gfloat zfar;
  gboolean ortho;

  /* user input */
  gdouble cursor_last_x;
  gdouble cursor_last_y;

  int pressed_mouse_button;

  gfloat center_distance;
  gfloat scroll_speed;
  gdouble rotation_speed;
  gfloat theta;
  gfloat phi;

  gfloat zoom_step;
  gfloat min_fov;
  gfloat max_fov;

  bool mouse_pressed;

  bool inverse_mode;
};

SphvrCameraDesktop *
sphvr_camera_desktop_new (void);

void
sphvr_camera_desktop_set_cursor_position (SphvrCameraDesktop *self, gdouble x,
                                          gdouble y);

void
sphvr_camera_desktop_update_view (SphvrCameraDesktop *self);

#endif /* SPHVR_CAMERA_DESKTOP_H_ */
