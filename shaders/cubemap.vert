/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#version 460 core

#extension GL_EXT_multiview : enable

layout (binding = 0) uniform Transformation
{
  mat4 mvp[2];
  float gamma;
  float unused;
  uint stereo_mode;
  uint phi_range;
}
ubo;

layout (location = 0) in vec4 in_position;
layout (location = 1) in vec2 in_uv;
layout (location = 2) in vec2 in_offset;

layout (location = 0) out vec2 uv_view;
layout (location = 1) out vec2 offset_view;

void
main ()
{
  uv_view = in_uv;
  offset_view = in_offset;
  gl_Position = ubo.mvp[gl_ViewIndex] * in_position;
}
