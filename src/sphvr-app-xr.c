/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include "sphvr-app.h"

#include "sphvr-camera-desktop.h"
#include "sphvr-enums.h"
#include "sphvr-image.h"
#include "sphvr-pipeline.h"
#include "sphvr-renderer.h"
#include "sphvr-video.h"

struct _SphvrAppXr
{
  SphvrApp parent;
};

G_DEFINE_TYPE (SphvrAppXr, sphvr_app_xr, SPHVR_TYPE_APP)

static void
sphvr_app_xr_init (SphvrAppXr *self)
{
  (void) self;
}

static void
_finalize (GObject *gobject)
{
  G_OBJECT_CLASS (sphvr_app_xr_parent_class)->finalize (gobject);
}

static gboolean
_iterate (SphvrApp *app)
{
  sphvr_renderer_draw (sphvr_app_get_renderer (app));
  return TRUE;
}

static gboolean
_inititalize (SphvrApp *app, const gchar *path)
{
  SphvrAppXr *self = SPHVR_APP_XR (app);

  SphvrRenderer *renderer = sphvr_app_get_renderer (SPHVR_APP (self));

  if (!sphvr_renderer_initialize (renderer))
    return FALSE;

  if (!sphvr_app_init_path (SPHVR_APP (self), path))
    return FALSE;

  sphvr_renderer_cache_draw_commands (renderer);

  return TRUE;
}

static void
sphvr_app_xr_class_init (SphvrAppXrClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;

  SphvrAppClass *sphvr_app_class = SPHVR_APP_CLASS (klass);
  sphvr_app_class->iterate = _iterate;
  sphvr_app_class->initialize = _inititalize;
}
