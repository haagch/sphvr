/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include <stdio.h>
#include <string.h>

#include <glib.h>
#include <gst/gst.h>

#include "sphvr-app.h"

static gchar *stereo_mode_str = NULL;
static gchar *projection_str = NULL;
static guint phi_range_degrees = 360;
static double gamma = 1.0;
static gboolean still_image = FALSE;

static gchar *output_str = NULL;

static const gchar *summary = "Examples:\n\n"
                              "View a equirect image\n"
                              "sphvr foo.jpg";

static GOptionEntry entries[] = {
  {"stereo", 's', 0, G_OPTION_ARG_STRING, &stereo_mode_str,
   "Stereo mode: none (default), side-by-side, top-bottom", NULL},
  {"phi-range", 'r', 0, G_OPTION_ARG_INT, &phi_range_degrees,
   "Phi range: 360 (default), 180", NULL},
  {"gamma", 'g', 0, G_OPTION_ARG_DOUBLE, &gamma,
   "Gamma correction: 1 (default)", NULL},
  {"image", 'i', 0, G_OPTION_ARG_NONE, &still_image,
   "Media is a still image. Default: not set", NULL},
  {"projection", 'p', 0, G_OPTION_ARG_STRING, &projection_str,
   "Projection type. One of: equirect (default), cube, eac", NULL},
  {"output", 'o', 0, G_OPTION_ARG_STRING, &output_str,
   "Output type. One of: desktop (default), xr", NULL},
  {NULL, 0, 0, 0, NULL, NULL, NULL},
};

int
main (int argc, char *argv[])
{
  GError *error = NULL;
  GOptionContext *context;

  context = g_option_context_new ("[FILE] - Render spherical media.");
  g_option_context_add_main_entries (context, entries, NULL);

  g_option_context_set_summary (context, summary);

  if (!g_option_context_parse (context, &argc, &argv, &error)) {
    g_printerr ("Error: %s\n", error->message);
    g_printerr ("%s", g_option_context_get_help (context, TRUE, NULL));
    return EXIT_FAILURE;
  }

  if (argc != 2) {
    g_printerr ("%s", g_option_context_get_help (context, TRUE, NULL));
    return EXIT_FAILURE;
  }

  /* Initialize GStreamer */
  gst_init (&argc, &argv);

  /* Parse settings */
  sphvr_stereo_mode stereo_mode = SPHVR_STEREO_NONE;
  if (stereo_mode_str != NULL) {
    if (g_strcmp0 (stereo_mode_str, "side-by-side") == 0) {
      stereo_mode = SPHVR_STEREO_SBS;
    } else if (g_strcmp0 (stereo_mode_str, "top-bottom") == 0) {
      stereo_mode = SPHVR_STEREO_TB;
    } else if (g_strcmp0 (stereo_mode_str, "none") == 0) {
    } else {
      g_printerr ("WARN: Unknown stereo mode: %s\n", stereo_mode_str);
    }
  }

  sphvr_projection_type projection_type = SPHVR_PROJECTION_EQUIRECT;
  if (projection_str != NULL) {
    if (g_strcmp0 (projection_str, "cube") == 0) {
      projection_type = SPHVR_PROJECTION_CUBE;
    } else if (g_strcmp0 (projection_str, "eac") == 0) {
      projection_type = SPHVR_PROJECTION_EAC;
    } else if (g_strcmp0 (projection_str, "equirect") == 0) {
    } else {
      g_printerr ("WARN: Unknown projection type: %s\n", projection_str);
    }
  }

  sphvr_output_type output_type = SPHVR_OUTPUT_DESKTOP;
  if (output_str != NULL) {
    if (g_strcmp0 (output_str, "desktop") == 0) {
      output_type = SPHVR_OUTPUT_DESKTOP;
    } else if (g_strcmp0 (output_str, "xr") == 0) {
      output_type = SPHVR_OUTPUT_XR;
    } else {
      g_printerr ("WARN: Unknown output type: %s\n", output_str);
    }
  }

  sphvr_φ_range φ_range;
  switch (phi_range_degrees) {
    case 180:
      φ_range = SPHVR_φ_180;
      break;
    case 360:
      φ_range = SPHVR_φ_360;
      break;
    default:
      g_printerr ("WARN: Unsupported φ range: %d\n", phi_range_degrees);
      φ_range = SPHVR_φ_360;
  }

  printf ("Stereo config: Mode %s Phi range: %s Gamma: %f Projection: %s "
          "Output: %s\n",
          sphvr_stereo_mode_str (stereo_mode), sphvr_φ_range_str (φ_range),
          gamma, sphvr_projection_type_str (projection_type),
          sphvr_output_type_str (output_type));

  SphvrApp *app = sphvr_app_new (output_type, gamma, stereo_mode,
                                 projection_type, φ_range, still_image);

  if (!sphvr_app_initialize (app, argv[1])) {
    g_object_unref (app);
    g_printerr ("Could not init app.\n");
    return EXIT_FAILURE;
  }

  sphvr_app_run (app);

  g_object_unref (app);

  return EXIT_SUCCESS;
}
